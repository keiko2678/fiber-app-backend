var express = require('express');
var router = express.Router();

var Item = require('../models/item.js');
var currentAPIversion = 2;
var lowestSupportedAPIversion = 1;

/*
Removes _id and __v from JSON objects
 */
function cleanItems(items) {
  items.forEach(function(item) {
      item.__v = undefined;
      item._id = undefined;
  })
};

/*
 Check API version for acceptance
 Checks if "apiVersion"in requests is supported by the api
 */
function apiVersionCheck(apiVersion, res) {
    if (apiVersion <= currentAPIversion &&
        apiVersion >= lowestSupportedAPIversion){
        return true;
    }else{
        res.json("Unnsported API version")
        return false;
    }
};

/*
Removes _id and __v from JSON object
 */
function cleanItem(item) {
    item.__v = undefined;
    item._id = undefined;
};

/*
GET all items in collection
 */
router.get('/', function(req, res){
    if(apiVersionCheck(req)){
        Item.find(function(err, items){
           if(err) {
               res.send(err);
               return;
           }
           cleanItems(items);
           res.json(items);
       });
    }
});

/*
GET one item from collection from itemId and type in url
 */
router.get('/:itemId/:type/:apiVersion', function(req, res){
    var itemId = req.params.itemId;
    var type = req.params.type;
    var apiVersion = req.params.apiVersion;
    if(apiVersionCheck(apiVersion)){
        Item.findOne({'itemId' :itemId, 'type' :type}, function(err, item){
            if(!item) {
                res.json("no match");
                return;
            }
            if(err) {
                res.send(err);
                return;
            }
            cleanItem(item);
            res.json(item);
        });
    }
});

/*
Accept POST request with json data
representing a new item
 */
router.post('/new', function(req, res){
    var i = new Item(req.body);
   // if(apiVersionCheck(req)){     // No api check for new items in the database
        i.save(function(err, new_item){
            if(err) {
                res.send('no new');
                return;
            }
            cleanItem(new_item);
            res.json(new_item);
        });
   // }
});

/*
UPDATE likeCount and dislikeCount on one item from json data
 */
router.put('/put/', function(req, res){
    var itemId = req.body.itemId;
    var type = req.body.type;
    var apiVersion = req.body.apiVersion;

    if(apiVersionCheck(apiVersion)){
        Item.findOne({'itemId' :itemId, 'type' :type}, function(err, item){
            if(!item) {
                res.json("no match");
                return;
            }
            if(err) {
                res.send(err);
                return;
            }
            item.likeCount = req.body.likeCount;
            item.dislikeCount = req.body.dislikeCount;
            item.save(function(err, updated_item){
                if(err){
                    res.json('no update');
                    return;
                }
                cleanItem(updated_item);
                res.json(updated_item);
            });
        });
    }
});
router.put('/update/:like/:dislike/', function(req, res){
    var itemId = req.body.itemId;
    var type = req.body.type;
    var like = req.params.like;
    var dislike = req.params.dislike;
    var apiVersion = req.body.apiVersion;

    if(apiVersionCheck(apiVersion)){
        Item.findOne({'itemId' :itemId, 'type' :type}, function(err, item){
            if(!item) {
                res.json("no match");
                return;
            }
            if(err) {
                res.send(err);
                return;
            }
            if(like == "L+"){
                item.likeCount +=1;

            } else if (like == "L-"){
                item.likeCount -=1;
            }
            if(dislike == "D+"){
                item.dislikeCount += 1;
            } else if(dislike == "D-"){
                item.dislikeCount -=1;
            }
            item.save(function(err, updated_item){
                if(err){
                    res.send('no update');
                    return;
                }
                cleanItem(updated_item);
                res.json(updated_item);
            });
        });
    }
});
module.exports = router;
