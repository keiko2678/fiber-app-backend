/**
 * Created by root on 21.11.14.
 */

var mongoose = require('mongoose');

var ItemSchema = mongoose.Schema({
    itemId : String,
    type: String,
    likeCount: Number,
    dislikeCount: Number
});

var Item = mongoose.model('Item', ItemSchema);
module.exports = Item;
